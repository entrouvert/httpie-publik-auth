#! /usr/bin/env python
# -*- coding: utf-8 -*-

import os
import subprocess

from distutils.command.sdist import sdist
from setuptools import setup


class eo_sdist(sdist):
    def run(self):
        if os.path.exists('VERSION'):
            os.remove('VERSION')
        version = get_version()
        version_file = open('VERSION', 'w')
        version_file.write(version)
        version_file.close()
        sdist.run(self)
        if os.path.exists('VERSION'):
            os.remove('VERSION')


def get_version():
    '''Use the VERSION, if absent generates a version with git describe, if not
       tag exists, take 0.0- and add the length of the commit log.
    '''
    if os.path.exists('VERSION'):
        with open('VERSION', 'r') as v:
            return v.read()
    if os.path.exists('.git'):
        p = subprocess.Popen(
            ['git', 'describe', '--dirty=.dirty', '--match=v*'],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        result = p.communicate()[0]
        if p.returncode == 0:
            result = result.decode('ascii').strip()[1:]  # strip spaces/newlines and initial v
            if '-' in result:  # not a tagged version
                real_number, commit_count, commit_hash = result.split('-', 2)
                version = '%s.post%s+%s' % (real_number, commit_count, commit_hash)
            else:
                version = result
            return version
        else:
            return '0.0.post%s' % len(subprocess.check_output(['git', 'rev-list', 'HEAD']).splitlines())
    return '0.0'


setup(
    name='httpie-publik-auth',
    version=get_version(),
    description='Publik auth plugin for HTTPie.',
    author='Emmanuel Cazenave',
    author_email='ecazenave@entrouvert.org',
    license='GPL',
    py_modules=['httpie_publik_auth'],
    zip_safe=False,
    entry_points={
        'httpie.plugins.auth.v1': [
            'httpie_publik_auth = httpie_publik_auth:PublikAuthPlugin'
        ]
    },
    install_requires=[
        'django>=1.11, <2.3',
        'httpie>=0.7.0'
    ],
    classifiers=[
        'Development Status :: 4 - Beta',
        'Programming Language :: Python',
        'Intended Audience :: Developers',
        'Environment :: Plugins',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Utilities'
    ],
    cmdclass={
        'sdist': eo_sdist,
    },

)
