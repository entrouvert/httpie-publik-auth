httpie-api-auth
===============

Publik auth plugin for `HTTPie <https://github.com/jkbr/httpie>`_.

Installation
------------

.. code-block:: bash

    $ pip install httpie-publik-auth

You should now see ``publik-auth`` under ``--auth-type`` in ``$ http --help`` output.

Usage
-----

.. code-block:: bash

    $ http --auth-type=publik-auth --auth='access_id:secret_key' example.org
